const joi = require ('joi')

const entrySchema = joi.object({
    API: joi.string(),
    Description: joi.string(),
    Auth: joi.string(),
    HTTPS: joi.boolean(),
    Cors: joi.string(),
    Link: joi.string(),
    Category: joi.string()
})

export default entrySchema;