# publicapis-test



## About

This is a test project for the https://api.publicapis.org/entries API.

## Test Cases

- Should validate all objects with Category equals to Authentication & Authorization

## Project structure

-----------------------

```
publicapis-test/
  ├─  cypress/
  │        │
  │        ├── contracts/
  │        │   ├── *.js
  │        │
  │        ├── e2e/
  │        │   ├── *.cy.js
  │        │
  │        ├── fixtures/
  │        │   └── *.json
  │        │
  │        ├── support/
  │        │   ├── commands.js
  │        │   └── e2e.js
  │        │  
  │        └── videos/
  │ 
  ├── node_modules/
  ├── reports/
  │      └── videos/
  │      └── mochafiles (*.html)
  ├── .gitlab-ci.yml
  ├── cypress.config.js
  ├── package-lock.json
  ├── package.json
  └── README.md
```

---------------------------------------

## Running the project on CI

1. On the top of the right side of the page, click on "Fork" button;
2. Fill in the required data to fork the project and confirm the fork;
3. In your project, in the left gitlab menu, access "Build" > "Pipelines";
4. On the top of the right side of the page, click on "Run Pipeline" button;
5. Keep the main branch selected and click on "Run pipeline" button;
6. When the pipeline finishes running, go back to the pipelines page ("Build" > "Pipelines") and click on the download button to download the test reports and video.

## Running the project locally

```
Note: to run this project locally you should have installed on your local machine:
- Git: You can download and install it from the official Git website (https://git-scm.com/). 
- Node.js: You can download and install it from the official Node.js website (https://nodejs.org/en/download).
``` 

1. On the right side of the page, click on "Clone" button;
2. Click on the "Copy URL" button within the "Clone with HTTPS" option;
3. On your local machine, on the desired directory, open a git bash terminal (right click with the mouse > Git bash here option) and type the command "git clone" followed by the URL of the project that you copy during step 2; run the command by clicking on enter button; this will create a copy of the project on your local machine;
4. After cloning the project, access it. You can do it either using the git bash terminal (run the command "cd [project-name]" - change the [project-name] to the name of the project that was cloned) or using one IDE (e.g. Vscode - open the IDE and open the project folder).
5. Run the command "npm install" (if you opened the project in one IDE, you have to open a new terminal in the project directory);
6. Run the command "npm run test"; you can check the tests result by accessing the results folder in the project.

## Author
Pâmela Inacio Almeida
